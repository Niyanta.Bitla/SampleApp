class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
    	t.string :mobile , null: false, default: ''
			t.string :address , null: false, default: ''
      t.timestamps
    end
  end
end
